{
    "item": {
        "only once": { "val": "0", "unit": "" },
        "every period A": { "val": "1", "unit": "" },
        "every period B": { "val": "2", "unit": "" },
        "every period C": { "val": "3", "unit": "" },
        "every period D": { "val": "4", "unit": "" },
        "every distance E": { "val": "5", "unit": "" },
        "every distance F": { "val": "6", "unit": "" },
        "every distance G": { "val": "7", "unit": "" },
        "every distance H": { "val": "8", "unit": "" },
        "still": { "val": "255", "unit": "" }
    },
    "default": "0"
}
