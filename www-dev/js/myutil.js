!function($) {
    "use strict";

    // private, class vars
    var Deploy = true;
    var NSandClock = 0;

    window.onerror = function(msg, url, line) {
        alert(msg + "\n" + url + ":" + line);
    };

    var MyUtil = function() {
        try {
            if ("undefined"== typeof document) return false;
            var path;
            if (Deploy) {
                path = '/var/sangomon/bin/';
            }
            else {
                path = '/var/sangomon/bin-tmp/';
            }
            this.pgJobs = path + 'jobs.py';
            this.pgNodes = path + 'nusage2.py';
            this.pgUsers = path + 'users.py';
            this.pgJobs4Nodes = path + 'jobs4nodes.py';
            this.pgPartusage = path + 'pusage2.py';
            this.pgNodes4Users = path + 'nodes4users.py';
            this.pgNodes4Jobs = path + 'nodes4jobs.py';
            this.pgShrinkNodenames = path + 'shrinkednodenames.py';
            this.pgJoblifetimes = path + 'joblifetimes.py';
            this.pgPartitions = path + 'partitions.py';
            this.pgDiskusage = path + 'diskusage.py';
            this.pgSacctlog = path + 'sacctlog.py';
            this.pgLsdir = path + 'lsdir.py';
            this.pgJobhist = path + 'jobhist.py';
            this.sacctlogdir = '/var/sangomon/log/sacct/jobs-completed';
            this.sangologdir = '/logdir';
            this.sangoStorageHomeLog = this.sangologdir + '/storage/home/quota_all.txt'
            this.sangoStorageWorkLog0 = this.sangologdir + '/storage/work/quota_all.txt'
            this.sangoStorageWorkLog1 = this.sangologdir + '/storage/work/user_usage.txt'
            this.sangoStorageBucketLog = this.sangologdir + '/storage/bucket/quota_all.txt'
            this.pgUserinfo = path + 'userinfo.py';
            this.mega = 1024 * 1024;
            this.giga = 1024 * 1024 * 1024;
        }
        catch (e) {
            console.log(e);
        }
    };

    MyUtil.prototype.byte2human = function(num, join) {
        var suf = ['', 'K', 'M', 'G', 'T', 'P'];
        var i = 0;
        while (num >= 1024 && i < suf.length - 1) {
            num /= 1024.0;
            i += 1;
        }
        var ret;
        if (join) {
            ret = Math.round(num) + suf[i];
        }
        else {
            ret = { "val": num, "suf": suf[i] };
        }
        return ret;
    };

    MyUtil.prototype.human2byte = function(sizestr) {
        var suf2mul = {
            'K': 1024,
            'M': 1024 * 1024,
            'G': 1024 * 1024 * 1024,
            'T': 1024 * 1024 * 1024 * 1024,
            'P': 1024 * 1024 * 1024 * 1024 * 1024,
        };
        var m = sizestr.match(/\s*(\d+(?:[.]\d*)?)(\w*)/);
        var num = m[1];
        var suf = m[2];
        if (suf) {
            num *= suf2mul[suf];
        }
        return num;
    };

    MyUtil.prototype.sort_hash_by_key = function(src, key, dir) {
        var rev = -1;
        if (dir == "ASC") {
            var rev = +1;
        }
        // first convert Obj 'src' into Array 'dst', then sort.
        var dst = [];
        for (var i = 0; i < src.length; i++) {
            dst[i] = src[i];
        }
        switch (key) {
        case "username": // string sort
        case "jobname":
            dst.sort(function(a, b) {
                if (a[key] < b[key]) return -1 * rev;
                if (a[key] > b[key]) return +1 * rev;
                return 0;
            });
            break;
        default: // integer sort
            dst.sort(function(a, b) {
                if (parseInt(a[key], 10) < parseInt(b[key], 10)) return -1 * rev;
                if (parseInt(a[key], 10) > parseInt(b[key], 10)) return +1 * rev;
                return 0;
            });
        }
        return dst;
    };

    MyUtil.prototype.show_sandclock = function() {
        NSandClock += 1;
        $('#indicator').show();
    };

    MyUtil.prototype.hide_sandclock = function() {
        NSandClock -= 1;
        if (NSandClock <= 0) {
            NSandClock = 0;
            $('#indicator').hide();
        }
    };

    MyUtil.prototype.query_to_backend = function(pgname, args, callback) {
        var my = this;
        this.show_sandclock();
        var fd = new FormData();
        fd.append("pgname", pgname);
        fd.append("args", args);
        var jqXHR = $.ajax({
            url: "./hm_backend.php",
            data: fd,
            type: "POST",
            processData: false,
            contentType: false,
            dataType: "json",
            timeout: 100000,
        }).done(function(res) {
            if (callback) {
                var promiseobj = callback(res);
                my.hide_sandclock();
                return promiseobj;
            }
            else {
                var $dfd = $.Deferred();
                $dfd.resolve();
                my.hide_sandclock();
                return $dfd.promise();
            }
        }).fail(function(jqXHR, status, thrown) {
            console.log("pgname:", pgname, " jqXHR:", jqXHR, " status:", status, " thrown:", thrown);
            my.hide_sandclock();
            var $dfd = $.Deferred();
            $dfd.resolve();
            return $dfd.promise();
        });
        return jqXHR;
    };

    MyUtil.prototype.jtlist = function(pgname, jtParams, state, userinfo) {
        var my = this;
        var partition = $("#partition-sel").val();
        if (! partition) {
            partition = "compute";
        }
        var args = partition + ' ' + state;
        var sparam = jtParams.jtSorting.split(' ');
        var $dfd = $.Deferred();
        this.query_to_backend(
            pgname, args,
            function(res) {
                var sres = my.sort_hash_by_key(res, sparam[0], sparam[1]);
                if (!userinfo.is_admin) {
                    sres = sres.map(function(elem, index, array){
                        if (elem.usergroup != userinfo.group) {
                            elem.username = '-';
                            elem.jobname = '-';
                        }
                        return elem;
                    });
                }

                var ret = {
                    "Result": "OK",
                    "Records": sres,
                    "TotalRecordCount": sres.length,
                };
                $dfd.resolve(ret);
            });
        return $dfd.promise();
    };

    MyUtil.prototype.jt_settings = function(fields, isSelectable, listaction, selectionchanged, loaded, sorting) {
        if (!sorting) {
            sorting = 'username ASC';
        }
        
        var settings = {
            paging: false,
            sorting: true,
            defaultSorting: sorting,
            selecting: true,
            multiselect: true,
            selectingCheckboxes: isSelectable,
            selectOnRowClick: false,
            actions: {
                listAction: listaction,
            },
            fields: fields,
            selectionChanged: selectionchanged,
            recordsLoaded: loaded,
        };
        return settings;
    };

    /*
    MyUtil.prototype.load_userinfo = function() {
        var $dfd = $.Deferred();
        var res = {};
        
        $.getJSON('userinfo.php', function(usr) {
            if (usr.group == 'scicomsec' || usr.group == 'infosys') {
                usr.is_admin = true;
            }
            res.user = usr;
            $dfd.resolve(res);
        });
        return $dfd.promise();
    };
     */
    
    // same as load_unserinfo but block until the result is obtained.
    MyUtil.prototype.sync_load_userinfo = function() {
        var res;

        var arg = {};
        var pair = location.search.substring(1).split('&');
        for (var i = 0; i < pair.length; i++) {
            var kv = pair[i].split('=');
            arg[kv[0]] = kv[1];
        }
        $.ajaxSetup({async: false})
        $.getJSON('userinfo.php', arg, function(usr) {
            if (usr.group == 'scicomsec' || usr.group == 'infosys') {
                usr.is_admin = true;
            }
            res = usr;
        });
        $.ajaxSetup({async: true})
        this.usr = res;
        return res;
    };

    // make Basic authentication fail to logout
    // by giving a wrong username/password.
    MyUtil.prototype.signout = function() {
        var pw = (new Date()).getTime().toString();
        return $.ajax({
            url: "index.html",
            type: 'GET',
            username: 'wrongname',
            password: pw,
        }).then(
            function (res) {
                // nop
            },
            function (res) {
                location.href = "index.html";
            }            
        );
    };

    MyUtil.prototype.load_partition = function() {
        var $dfd = $.Deferred();
        var my = this;
        if (my.partitionInfo) { // resuse if already loaded.
            $dfd.resolve(my.partitionInfo);
        }
        else {
            my.query_to_backend(my.pgPartitions, '-u ' + my.usr.name, function(res) {
                var res0 = res[0]
                var p = {};
                for (var i = 0; i < res0.length; i++) {
                    p[res0[i].name] = {
                        name: res0[i].name,
                        ncpu: res0[i].ncpu,
                        mem: res0[i].mem,
                        nnode: res0[i].nnode,
                    };
                }
                my.partitionInfo = p;
                $dfd.resolve(p);
            });
        }
        return $dfd.promise();
    }

    MyUtil.prototype.load_partitionmenu = function($select) {
        var $dfd = $.Deferred();
        this.load_partition().then(function(p){
            for (var pname in p) {
                $select.append($("<option>") .html(pname) .val(pname));
            }
            $select.val("compute");
            $dfd.resolve(p);
        });
        return $dfd.promise();
    }

    MyUtil.prototype.load_sacctlogmenu = function($select) {
        var $dfd = $.Deferred();
        this.query_to_backend(this.pgLsdir, this.sacctlogdir, function(res) {
            var monthname = ['',
                             'Jan', 'Feb', 'Mar', 'Apr',
                             'May', 'Jun', 'Jul', 'Aug',
                             'Sep', 'Oct', 'Nov', 'Dec',
                            ];
            var res0 = res[0];
            for (var i = 0; i < res0.length; i++) {
                var h = res0[i];
                if (!h.match(/2\d{7}[-]2\d{7}/)) continue; // filter file with name like '20170801-20170831'.
                var y = h.slice(0, 4);
                var m = parseInt(h.slice(4, 6));
                $select.append($("<option>").html(monthname[m] + ',' + y).val(res0[i]));
            }
            $dfd.resolve();
        });
        return $dfd.promise();
    }
    MyUtil.prototype.load_barchart = function(id, fraction) {
        var width = $(id).attr('width');
        var height = $(id).attr('height');
        $(id).css('width', width + 'px');
        $(id).css('padding', '1px');
        var h1 = parseInt(height, 10) + 4;
        var w1 = parseInt(width, 10) + 4;
        $(id).css('height', h1);
        $(id).css('width', w1);
        
        d3.select(id).select("div").remove();
        var bc = d3.select(id).selectAll(id).data([ {} ]);
        bc.enter().append("div").attr("class", "barchart-path");
        
        d3.select(id).selectAll(".barchart-path").append("div")
            .attr("class", "barchart-bgimg")
            .style('background-size', width + 'px')
            .style('height', height + 'px');
        d3.select(id).selectAll(".barchart-path")
            .style("height", height + 'px')
            .style("width", function(d, i) {
                return width * fraction + "px"; 
            });
    }

    MyUtil.prototype.date_to_string = function(date, format) {
        var monthname = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
        ];
        var dayofweekname = [
            'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
        ];

        if (!format) format = '%Y/%m/%d %H:%M:%S';

        format = format.replace(/%Y/g, date.getFullYear());
        format = format.replace(/%m/g, ('0' + (date.getMonth() + 1)).slice(-2));
        format = format.replace(/%d/g, ('0' + date.getDate()).slice(-2));
        format = format.replace(/%H/g, ('0' + date.getHours()).slice(-2));
        format = format.replace(/%M/g, ('0' + date.getMinutes()).slice(-2));
        format = format.replace(/%S/g, ('0' + date.getSeconds()).slice(-2));
        format = format.replace(/%b/g, monthname[date.getMonth()]);
        format = format.replace(/%a/g, dayofweekname[date.getDay()]);
        return format;
    };

    MyUtil.prototype.seconds_ago = function(sec) {
        var now = new Date();
        var date = new Date();
        date.setTime(now.getTime() - sec * 1000);
        return date;
    }

    // truncate src to the nearest dsrc aligned value smaller than src.
    MyUtil.prototype.align_floor = function(src, dsrc) {
        var dst = (Math.floor(src / dsrc)) * dsrc;
        //    console.log("src:" + src + " dst:" + dst + " dsrc:" + dsrc);
        return dst;
    }

    // round up src to the nearest dsrc aligned value larger than src.
    MyUtil.prototype.align_ceil = function(src, dsrc) {
        var dst = (Math.ceil(src / dsrc)) * dsrc;
        //    console.log("src:" + src + " dst:" + dst);
        return dst;
    }

    MyUtil.prototype.load_timeplot = function(id, data, range) {

    }

    MyUtil.prototype.sandclock_html = function() {
        var h = '<div id="sandclock" align="center" style="position:absolute; top:100px; left:200px;">'
        h += '<img src="./images/indicator.gif">';
        h += '</div>';
        return h;
    }

    MyUtil.prototype.load_msg = function(name) {
        return $.ajax({
            type: "POST",
            url: "load_message.php",
            data: JSON.stringify({ 
                name: name,
            }),
            contentType: 'application/json',
            dataType: "text",
        });
    }
    
    // export the constructor
    window.MyUtil = MyUtil;
}(jQuery)
