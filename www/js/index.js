!function($) {
    "use strict";
    var ADDR_MAX = 0x40;  // addr:10000000..1003F000
    var NMSG_ITEMS = 24;  // addr:10014000..1001F000
    var NTIMER_ITEMS = 8; // addr:10010000..10013000
    var MSG_ADDR_OFF = 0x14;
    var TIMER_ADDR_OFF = 0x10;
    var Param = { "items":[], "default":null, };
    var Phase = { "items":[], "default":null, };
    var Field = { "items":[], "default":null, };
    var Comp = { "items":[], "default":null, };
    var Say = { "items":[], "default":null, };
    var Format = { "items":[], "default":null, };
    var Period = { "items":[], "default":null, };
    var Do = { "items":[], "default":null, };
    var Memory = null;

    init_model().then(function(){
        init_view();
    });

    function set_items(src, dst) {
        for (var key in src.item) {
            dst.items.push({ key: key,
                             val: src.item[key]['val'],
                             unit: src.item[key]['unit'],
                             factor: src.item[key]['factor'],
                           });
        }
        dst.default = src.default;
    }

    function init_model() {
        var inputs = [
            { "file": "./param.txt", "buf": Param },
            { "file": "./phase.txt", "buf": Phase },
            { "file": "./field.txt", "buf": Field },
            { "file": "./comp.txt", "buf": Comp },
            { "file": "./format.txt", "buf": Format },
            { "file": "./say.txt", "buf": Say },
            { "file": "./period.txt", "buf": Period },
            { "file": "./do.txt", "buf": Do },
        ];
        var $dfd = $.Deferred();
        var $dfds = [];
        inputs.forEach(function(input) {
            var $d = new $.Deferred();
            $.getJSON(input.file).then(function(res){
                set_items(res, input.buf);
                $d.resolve();
            });
            $dfds.push($d.promise());
        });
        $.when.apply($, $dfds).then(function(res){
            // alloc & init Memory.
            var mem = Memory = new Array(ADDR_MAX);
            for (var i = 0; i < mem.length; i++) {
                mem[i] = new Array(16);
                for (var j = 0; j < 16; j++) {
                    mem[i][j] = 0;
                }
            }
            $dfd.resolve();
        });
        return $dfd.promise();
    }

    function init_view() {
        var msg_colmns = [
            { name: "id", title: "#", type: "number", align: "right", width:1, editing: false, },

            { name: "phase", title: "Phase", type: "select", items: Phase.items, valueType: "text", valueField: "val",
              textField: "key", width: 50, align: "left", editing: true, sorting: false, css: "phase", },

            { name: "field", title: "Field", type: "select", items: Field.items, valueType: "text", valueField: "val",
              textField: "key", width: 50, align: "left", editing: true, sorting: false, css: "field", },

            { name: "comp", title: "Comp", type: "select", items: Comp.items, valueType: "text", valueField: "val",
              textField: "key", width: 1, align: "center", editing: true, sorting: false, css: "comp", },

            { name: "threshold", title: "Threshold", type: "text", align: "right", width:20, editing: true, },

            { name: "say", title: "Say", type: "select", items: Say.items, valueType: "text", valueField: "val",
              textField: "key", width: 50, align: "left", editing: true, sorting: false, css: "say", },

            { name: "format", title: "format", type: "select", items: Format.items, valueType: "text", valueField: "val",
              textField: "key", width: 50, align: "left", editing: true, sorting: false, css: "format", },

            { name: "period", title: "Period", type: "select", items: Period.items, valueType: "text", valueField: "val",
              textField: "key", width: 50, align: "left", editing: true, sorting: false, css: "period", },

            { type: "control", deleteButton: false, name: "edit", title: "", width: 10 },
        ];

        var timer_colmns = [
            { name: "id", title: "#", type: "number", align: "right", width:1, editing: false, },

            { name: "phase", title: "Phase", type: "select", items: Phase.items, valueType: "text", valueField: "val",
              textField: "key", width: 50, align: "left", editing: true, sorting: false, css: "phase", },

            { name: "field", title: "Field", type: "select", items: Field.items, valueType: "text", valueField: "val",
              textField: "key", width: 50, align: "left", editing: true, sorting: false, css: "field", },

            { name: "comp", title: "Comp", type: "select", items: Comp.items, valueType: "text", valueField: "val",
              textField: "key", width: 1, align: "center", editing: true, sorting: false, css: "comp", },

            { name: "threshold", title: "Threshold", type: "text", align: "right", width:20, editing: true, },

            { name: "do", title: "Do", type: "select", items: Do.items, valueType: "text", valueField: "val",
              textField: "key", width: 50, align: "left", editing: true, sorting: false, css: "do", },

            { name: "init", title: "Init", type: "text", align: "right", width:20, editing: true, },

            { type: "control", deleteButton: false, name: "edit", title: "", width: 10 },
        ];

        var param_colmns = [
            { name: "key", title: "Name", type: "text", align: "left", width:50, editing: false, },

            // { name: "val", title: "Value", type: "text", align: "right", width:50, editing: true, },
            { name: "val", title: "Value", type: "text", align: "right", width:50, editing: true, },

            { name: "unit", title: "Unit", type: "text", align: "left", width:5, editing: false, },
            { type: "control", deleteButton: false, name: "edit", title: "", width: 10 },
        ];

        $('#plnr-msg-form').jsGrid({
            height: "200",
            width: "100%",
            filtering: false,
            editing: true,
            inserting: false,
            sorting: false,
            paging: false,
            autoload: true,
            controller: {
                loadData: msg_loader,
                updateItem: msg_updater,
            },
            fields: msg_colmns,
            onDataLoaded: function(args) {
                update_hex();
            }
        });

        $('#plnr-timer-form').jsGrid({
            height: "200",
            width: "100%",
            filtering: false,
            editing: true,
            inserting: false,
            sorting: false,
            paging: false,
            autoload: true,
            controller: {
                loadData: timer_loader,
                updateItem: timer_updater,
            },
            fields: timer_colmns,
            onDataLoaded: function(args) {
                update_hex();
            }
        });

        $('#plnr-param-form').jsGrid({
            height: "400",
            width: "100%",
            filtering: false,
            editing: true,
            inserting: false,
            sorting: false,
            paging: false,
            autoload: true,
            controller: {
                loadData: param_loader,
                updateItem: param_updater,
            },
            fields: param_colmns,
            onDataLoaded: function(args) {
                update_hex();
            }
        });

    }

    function write_msg_item_to_mem(item) {
        var mem = Memory;
        var adr = (item.id >> 1) + MSG_ADDR_OFF
        var off = item.id % 2 ? 0 : 8;
        write_phase_field_comp_to_mem(item, adr, off);
        mem[adr][off + 5] = parseInt(item.say, 10);
        mem[adr][off + 6] = parseInt(item.format, 10);
        mem[adr][off + 7] = parseInt(item.period, 10);
    }
    
    function write_timer_item_to_mem(item) {
        var mem = Memory;
        var adr = (item.id >> 1) + TIMER_ADDR_OFF;
        var off = item.id % 2 ? 0 : 8;
        write_phase_field_comp_to_mem(item, adr, off);
        mem[adr][off + 5] = parseInt(item.do, 10);
        mem[adr][off + 6] = parseInt(item.init, 10);
    }

    function write_phase_field_comp_to_mem(item, adr, off) {
        var mem = Memory;
        mem[adr][off + 0] = parseInt(item.phase, 10);
        mem[adr][off + 1] = parseInt(item.field, 10);
        mem[adr][off + 2] = parseInt(item.comp, 10);

        var field = $.grep(Field.items, function(elem, index){
            return elem.val == item.field;
        })[0];
        var titem = {
            "val":item.threshold.toString(),
            "factor": field.factor,
        };
        write_int2b(titem, adr, off + 3);
    }

    function write_string(item, addr, nline) {
        var cnt = 0;
        for (var a = addr; a < addr + nline; a++) {
            var mem =Memory[a];
            var str = item.val.slice(cnt * 16 + 0, cnt * 16 + 16);
            cnt += 1;
            for (var i = 0; i < 16; i++) {
                mem[i] = 0;
                if (i < str.length) {
                    mem[i] = str.charCodeAt(i);
                }
            }
        }
    }

    function write_int1b(item, addr, off) {
        var val = parseFloat(item.val);
        if (item.factor) {
            var factor = parseFloat(item.factor);
            val = Math.round(val * factor);
        }
        if (val < 0) val += 256;
        Memory[addr][off] = val % 256;
    }

    function write_int2b(item, addr, off) {
        var val = parseFloat(item.val);
        if (item.factor) {
            var factor = parseFloat(item.factor);
            val = Math.round(val * factor);
        }
        if (val < 0) val += 256 * 256;
        Memory[addr][off + 1] = Math.trunc(val / 256); // higher byte
        Memory[addr][off] = val % 256; // lower byte
        // console.log('mem[0x%s][0x%s]:0x%s', addr.toString(16), off.toString(16), Memory[addr][off].toString(16));
        // console.log('mem[0x%s][0x%s]:0x%s', addr.toString(16), (off + 1).toString(16), Memory[addr][off + 1].toString(16));
    }

    var Param_setter = {
        "version": function(item) {
            var val32 = parseInt(item.val, 16);
            var mem0 = Memory[0];
            mem0[0] = (val32 >> 24) & 0xff;
            mem0[1] = (val32 >> 16) & 0xff;
            mem0[2] = (val32 >> 8) & 0xff;
            mem0[3] = (val32 >> 0) & 0xff;
        },
        "author": function(item) {
            write_string(item, 0x1, 1);
        },
        "title": function(item) {
            write_string(item, 0x2, 2);
        },
        "description": function(item) {
            write_string(item, 0x20, 32);
        },
        "sample_rate": function(item) {
            write_int1b(item, 0xc, 0);
            var val8 = parseInt(item.val, 10);
            Memory[0x8][0] = Math.trunc(150 / val8);
        },
        "suspend_after": function(item) {
            write_int2b(item, 0x8, 2);
        },
        "suspend_mode_sample_rate": function(item) {
            write_int1b(item, 0xc, 4);
            var val8 = parseInt(item.val, 10);
            Memory[0x8][4] = Math.trunc(5 + Math.log(val8)/Math.log(2));
        },
        "dive_start_depth_threshold": function(item) {
            write_int1b(item, 0x8, 8);
        },
        "surfacing_depth_threshold": function(item) {
            write_int1b(item, 0x8, 10);
        },
        "ascend_threshold": function(item) {
            write_int2b(item, 0x9, 0);
        },
        "bottom_threshold": function(item) {
            write_int2b(item, 0x9, 2);
        },
        "salinity": function(item) {
            write_int1b(item, 0x9, 8);
        },
        "temp_source": function(item) {
            write_int1b(item, 0x9, 0xa);
        },
        "fixed_temp": function(item) {
            write_int2b(item, 0x9, 0xc);
        },
        "AMSL": function(item) {
            write_int2b(item, 0xd, 13);
            var amsl = parseFloat(item.val);
            write_gravity();
        },
        "Latitude": function(item) {
            write_int1b(item, 0xd, 15);
            var latitude = parseFloat(item.val);
            write_gravity();
        },
        "speaker_volume": function(item) {
            write_int1b(item, 0x8, 1);
        },
        "period_A": function(item) {
            write_int1b(item, 0xf, 0);
        },
        "period_B": function(item) {
            write_int1b(item, 0xf, 1);
        },
        "period_C": function(item) {
            write_int1b(item, 0xf, 2);
        },
        "period_D": function(item) {
            write_int1b(item, 0xf, 3);
        },
        "distance_E": function(item) {
            write_int2b(item, 0xf, 8);
        },
        "distance_F": function(item) {
            write_int2b(item, 0xf, 10);
        },
        "distance_G": function(item) {
            write_int2b(item, 0xf, 12);
        },
        "distance_H": function(item) {
            write_int2b(item, 0xf, 14);
        },
        "end_of_param": function(item) { /* nop */ },
    };

    function write_gravity() {
        var amsl = parseFloat(
            $.grep(Param.items, function(elem, index){
                return elem.key == 'AMSL';
            })[0].val);
        var latitude = parseFloat(
            $.grep(Param.items, function(elem, index){
                return elem.key == 'Latitude';
            })[0].val);

        // latitude = amsl = 0.0;
        var rad = 2.0 * Math.PI / 360.0;
        var sinL = Math.sin(latitude * rad);
        var sin2L = Math.sin(2.0 * latitude * rad);
        var gravity = Math.trunc(128.0 * 256.0 *
                        (9.780318 *
                         (1.0 + 0.0053024 * (sinL * sinL)
                          - 0.0000058 * (sin2L * sin2L)
                          - 0.000003086 * amsl
                         ) / 10.0
                        ));
        // console.log('amsl:', amsl, ' latitude:', latitude, ' gravity:', gravity);
        var gitem = {
            "val": gravity,
        };
        write_int2b(gitem, 0x9, 14);

        // Memory[9][14] = parseInt(item.val, 16);
        // Memory[9][15] = parseInt(item.val, 16);
        // "unknown0": { "val": "0x84", "unit": "" },
        // "unknown1": { "val": "0x7d", "unit": "" }
        // 7d84   32132
        // 04e7   1255
        // 04e3   1251
    }

    function write_param_item_to_mem(item) {
        // update Param with the value changed on the jsgreid field.
        $.grep(Param.items, function(elem, index){
            return elem.key == item.key;
        })[0].val = item.val;
        // write the updated value to Memory.
        Param_setter[item.key](item);
        update_hex();
    }

    var Msg_loader_arg = {
        "init": true,
    };

    function msg_loader() {
        if (Msg_loader_arg.init) {
            return init_msg_items();
        }
        else {
            return read_msg_items_from_mem();
        }
    }

    var Timer_loader_arg = {
        "init": true,
    };

    function timer_loader() {
        if (Timer_loader_arg.init) {
            return init_timer_items();
        }
        else {
            return read_timer_items_from_mem();
        }
    }

    var Param_loader_arg = {
        "init": true,
    };

    function param_loader() {
        if (Param_loader_arg.init) {
            return init_param_items();
        }
        else {
            return read_param_items_from_mem();
        }
    }

    function init_msg_items() {
        var ret = [];
        for (var i = 0; i < NMSG_ITEMS; i++) {
            var item = {
                "id": i,
                "phase": Phase.default,
                "field": Field.default,
                "comp": Comp.default,
                "threshold": 0,
                "say": Say.default,
                "format": Format.default,
                "period": Period.default,
            };
            ret.push(item);
            write_msg_item_to_mem(item);
        }
        return ret;
    }

    function init_timer_items() {
        var ret = [];
        for (var i = 0; i < NTIMER_ITEMS; i++) {
            var item = {
                "id": i,
                "phase": Phase.default,
                "field": Field.default,
                "comp": Comp.default,
                "threshold": 0,
                "do": Do.default,
                "init": 0,
            };
            ret.push(item);
            write_timer_item_to_mem(item);
        }
        return ret;
    }

    function init_param_items() {
        var ret = [];
        var params = Param.items;
        for (var i = 0; i <params.length; i++) {
            var item = {
                "key": params[i].key,
                "val": params[i].val,
                "unit": params[i].unit,
                "factor": params[i].factor,
            };
            if (item.factor) {
                var val = parseFloat(item.val);
                var factor = parseFloat(item.factor);
                item.val = (val / factor).toString();
            }
            ret.push(item);
            write_param_item_to_mem(item);
        }
        return ret;
    }

    function read_msg_items_from_mem() {
        var ret = [];
        for (var i = 0; i < NMSG_ITEMS; i++) {
            var item = {
                "id": i,
            };
            read_msg_item_from_mem(item);
            ret.push(item);
        }
        return ret;
    }

    function read_timer_items_from_mem() {
        var ret = [];
        for (var i = 0; i < NTIMER_ITEMS; i++) {
            var item = {
                "id": i,
            };
            read_timer_item_from_mem(item);
            ret.push(item);
        }
        return ret;
    }

    function read_param_items_from_mem() {
        var ret = [];
        var params = Param.items;
        for (var i = 0; i <params.length; i++) {
            var item = {
                "key": params[i].key,
                "val": null,
                "unit": params[i].unit,
                "factor": params[i].factor,
            };
            read_param_item_from_mem(item);
            ret.push(item);
        }
        return ret;
    }

    var Param_getter = {
        "version": function(item) {
            var mem0 = Memory[0];
            var val = '';
            val += mem0[0].toString(16);
            val += mem0[1].toString(16);
            val += mem0[2].toString(16);
            val += mem0[3].toString(16);
            item.val = val;
        },
        "author": function(item) {
            read_string(item, 0x1, 1);
        },
        "title": function(item) {
            read_string(item, 0x2, 2);
        },
        "description": function(item) {
            read_string(item, 0x20, 32);
        },
        "sample_rate": function(item) {
            read_int1b(item, 0xc, 0);
        },
        "suspend_after": function(item) {
            read_int2b(item, 0x8, 2);
        },
        "suspend_mode_sample_rate": function(item) {
            read_int1b(item, 0xc, 4);
        },
        "dive_start_depth_threshold": function(item) {
            read_int1b(item, 0x8, 8);
        },
        "surfacing_depth_threshold": function(item) {
            read_int1b(item, 0x8, 10);
        },
        "ascend_threshold": function(item) {
            read_int2b(item, 0x9, 0);
        },
        "bottom_threshold": function(item) {
            read_int2b(item, 0x9, 2);
        },
        "salinity": function(item) {
            read_int1b(item, 0x9, 8);
        },
        "temp_source": function(item) {
            read_int1b(item, 0x9, 0xa);
        },
        "fixed_temp": function(item) {
            read_int2b(item, 0x9, 0xc);
        },
        "AMSL": function(item) {
            read_int2b(item, 0xd, 13);
        },
        "Latitude": function(item) {
            read_int1b(item, 0xd, 15);
        },
        "speaker_volume": function(item) {
            read_int1b(item, 0x8, 1);
        },
        "period_A": function(item) {
            read_int1b(item, 0xf, 0);
        },
        "period_B": function(item) {
            read_int1b(item, 0xf, 1);
        },
        "period_C": function(item) {
            read_int1b(item, 0xf, 2);
        },
        "period_D": function(item) {
            read_int1b(item, 0xf, 3);
        },
        "distance_E": function(item) {
            read_int2b(item, 0xf, 8);
        },
        "distance_F": function(item) {
            read_int2b(item, 0xf, 10);
        },
        "distance_G": function(item) {
            read_int2b(item, 0xf, 12);
        },
        "distance_H": function(item) {
            read_int2b(item, 0xf, 14);
        },
        "unknown0": function(item) {
            item.val = Memory[9][14];
        },
        "unknown1": function(item) {
            item.val = Memory[9][15];
        },
        "end_of_param": function(item) { /* nop */ },
    };

    function read_param_item_from_mem(item) {
        Param_getter[item.key](item);
    }

    function read_msg_item_from_mem(item) {
        var mem = Memory;
        var adr = (item.id >> 1) + MSG_ADDR_OFF
        var off = item.id % 2 ? 0 : 8;
        read_phase_field_comp_from_mem(item, adr, off);
        item.say = mem[adr][off + 5].toString(10);
        item.format = mem[adr][off + 6].toString(10);
        item.period = mem[adr][off + 7].toString(10);
    }

    function read_timer_item_from_mem(item) {
        var mem = Memory;
        var adr = (item.id >> 1) + TIMER_ADDR_OFF
        var off = item.id % 2 ? 0 : 8;
        read_phase_field_comp_from_mem(item, adr, off);
        item.do = mem[adr][off + 5].toString(10);
        item.init = mem[adr][off + 6].toString(10);
    }

    function read_phase_field_comp_from_mem(item, adr, off) {
        var mem = Memory;
        item.phase = mem[adr][off + 0].toString(10);
        item.field = mem[adr][off + 1].toString(10);
        item.comp = mem[adr][off + 2].toString(10);

        var field = $.grep(Field.items, function(elem, index){
            return elem.val == item.field;
        })[0];
        var titem = {
            "factor": field.factor,
        };
        read_int2b(titem, adr, off + 3);
        item.threshold = titem.val;
    }

    function read_string(item, addr, nline) {
        var eos = false;
        var val = '';
        for (var a = addr; (a < addr + nline) && !eos; a++) {
            var mem =Memory[a];
            for (var i = 0; i < 16; i++) {
                if (mem[i] == 0) {
                    eos = true;
                    break;
                }
                val += String.fromCharCode(mem[i]);
            }
        }
        item.val = val;
    }

    function read_int1b(item, addr, off) {
        var val = Memory[addr][off];
        // console.log('adr:', addr, ' off:', off, ' val:', val);
        if (127 < val) val -= 256;
        if (item.factor) val /= item.factor;
        item.val = val;
    }

    function read_int2b(item, addr, off) {
        var hi = Memory[addr][off + 1];
        var lo = Memory[addr][off];
        var val = hi * 256 + lo;
        if (256 * 127 < val) val -= 256 * 256;
        if (item.factor) val /= item.factor;
        item.val = val;
    }

    function msg_updater(item) {
        write_msg_item_to_mem(item);
        update_hex();
    }

    function timer_updater(item) {
        write_timer_item_to_mem(item);
        update_hex();
    }

    function param_updater(item) {
        write_param_item_to_mem(item);
        update_hex();
    }

    function dump_memory(mem) {
        var res = [];
        var bytecnt = 0x10;
        var type_data = 0x00;
        var type_eof = 0x01;
        var row = new Array(1 + 2 + 1 + 16 + 1); // byte count"1, address:2, record type:1, data:16, checksum:1
        for (var i = 0; i < mem.length; i++) {
            row[0] = bytecnt;
            row[1] = (i >> 4);
            row[2] = (i << 4) & 0xff;
            row[3] = type_data;
            for (var j = 0; j < 16; j++) {
                row[j + 4] = mem[i][j];
            }
            var cksum = 0x100;
            for (var j = 0; j < row.length - 1; j++) {
                cksum -= row[j];
            }
            row[row.length - 1] = (cksum >>> 0); // make cksum an unsigned num.
            var rowstr = ':';
            for (var j = 0; j < row.length; j++) {
                rowstr += ('00' + row[j].toString(16)).slice(-2);
            }
            res.push((rowstr.slice(0, 9) + ' ' + rowstr.slice(9, -2) + ' ' + rowstr.slice(-2)).toUpperCase());
        }
        res.push(":00000001FF\r\n"); // EOF
        return res;
    }

    function update_hex() {
        var hex = dump_memory(Memory);
        var h = '';
        for (var i = 0; i < hex.length; i++) {
            h += hex[i] + '<br/>';
        }
        $('#plnr-hex-text').html(h);
    };

    $('#plnr-hex-upload-hidden').change(function(event){
        var file = event.target.files[0];
        var reader = new FileReader();
        reader.readAsText(file);
        reader.onload = function(f){
            var text = f.target.result;
            var hex = text.split(/\n/)
            for (var i = 0; i < hex.length; i++) {
                var m = hex[i].match(/\:\w{3}(\w{2})\w{3}(\w{32})\w{2}/);
                if (!m) continue; // the last line
                var adr = parseInt(m[1], 16);
                for (var off = 0; off < 16; off++) {
                    var val8 = parseInt(m[2].slice(off * 2, off * 2 + 2), 16);
                    Memory[adr][off] = val8;
                }
            }
            update_hex();

            Msg_loader_arg.init = false;
            $('#plnr-msg-form').jsGrid('loadData');

            Timer_loader_arg.init = false;
            $('#plnr-timer-form').jsGrid('loadData');

            Param_loader_arg.init = false;
            $('#plnr-param-form').jsGrid('loadData');
        };
    });

    $('#plnr-hex-upload').on('click', function() {
        $('#plnr-hex-upload-hidden').click();
    });

    $('#plnr-hex-download').click(function() {
        var hex = dump_memory(Memory);
        var content = '';
        for (var i = 0; i < hex.length; i++) {
            content += hex[i].replace(/\s/g, '') + "\r\n";
        }
        var fname = 'output.roo';
        var link = document.createElement( 'a' );
        link.href = window.URL.createObjectURL( new Blob( [content] ) );
        link.download = fname;
        link.click();
    });

}(jQuery);
