{
    "item": {
        "version": { "val": "01000001", "unit": "" },
        "author": { "val": "MAVI", "unit": "" },
        "title": { "val": "test", "unit": "" },
        "description": { "val": "test", "unit": "" },
        "sample_rate": { "val":  "1", "unit": "Hz" },
        "suspend_after": { "val":  "1800", "unit": "s" },
        "suspend_mode_sample_rate": { "val":  "2", "unit": "s" },
        "dive_start_depth_threshold": { "val": "100", "unit": "m", "factor": "100" },
        "surfacing_depth_threshold": { "val": "20", "unit": "m", "factor": "100" },
        "ascend_threshold": { "val": "-80", "unit": "m/s", "factor": "100" },
        "bottom_threshold": { "val": "-10", "unit": "m/s", "factor": "100" },
        "salinity": { "val": "30", "unit": "‰" },
        "temp_source": { "val": "0", "unit": "{0:fixed, 1:min, 2:avg, 3:current}" },
        "fixed_temp": { "val": "41", "unit": "C", "factor": "10" },
        "AMSL": { "val": "0", "unit": "m", "factor": "100" },
        "Latitude": { "val": "45", "unit": "deg" },
        "speaker_volume": { "val": "0", "unit": "dB" },
        "period_A": { "val": "5", "unit": "s" },
        "period_B": { "val": "1", "unit": "s" },
        "period_C": { "val": "1", "unit": "s" },
        "period_D": { "val": "1", "unit": "s" },
        "distance_E": { "val": "100", "unit": "m", "factor": "100" },
        "distance_F": { "val": "500", "unit": "m", "factor": "100" },
        "distance_G": { "val": "1000", "unit": "m", "factor": "100" },
        "distance_H": { "val": "2000", "unit": "m", "factor": "100" }
    }
}
